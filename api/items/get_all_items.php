<?php
    include("../connect.php");
    
    $sql = "SELECT * FROM items";
    $results = mysqli_query($conn, $sql);
    $itemsArray = array();

    while($row = $results->fetch_assoc()) {
        $itemsArray[] = $row;
    }

    echo json_encode($itemsArray);
?>