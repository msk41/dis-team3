<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
// header("Access-Control-Allow-Methods: GET");

require_once "db_config.php";

$qry = "SELECT * FROM items";
$results = mysqli_query($conn, $qry) OR die ("The Query Failed!");
$count = mysqli_num_rows($results);

if ($count > 0){
    $row = mysqli_fetch_all($results, MYSQLI_ASSOC);
    echo json_encode($row);
}
else{
    echo json_encode(array("message" => "No Product Found", "status" => false));
}
?>