<?php
    header("Content-Type: application/json");
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT");
    header("Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization");

    $data = json_decode(file_get_contents("php://input"), true);
    require_once "db_config.php";

    $cust_id=$data['id'];
    $name=$data['name'];
    $lname=$data['last_name'];
    $email=$data['email'];
    $phone=$data['phone'];
    $gender=$data['gender'];
    $age=$data['age'];
    $location=$data['location'];
    $total_spent=$data['total_spent'];
    $total_visit=$data['total_visit'];

    $sql = "UPDATE customers SET name='$name', last_name='$lname', email='$email', phone='$phone', gender='$gender', 
    age='$age', location='$location' WHERE id='$cust_id'";

    echo $sql;
    if(mysqli_query($conn,$sql))
    {
        echo json_encode(array("message" => "The customer has been updated", "status" => true));
    }
    else{
        echo json_encode(array("message" => "Unable to update customer", "status" => false));
    }
?>