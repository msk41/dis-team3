<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tag Along</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="./TagAlong/style.css">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/sidebars/">
    <script src="https://kit.fontawesome.com/7181215bbd.js" crossorigin="anonymous"></script>
    

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    
    <!-- Custom styles for this template -->
    <link href="sidebars.css" rel="stylesheet">
  </head>
  <body>
    <section>
    <div class="nav-container">
        
      <ul class="nav nav-pills flex-column flex-sm-row">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="./about.html">About Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Trips</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./hostels.html">Hostels</a>
        </li>
        <li class="nav-item">
          <a class="nav-link">Contact Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./login.html">Log In</a>
        </li>
        
      </ul>
    </div>
    </section>

<main>

  <div class="d-flex flex-column flex-shrink-0 p-3 bg-light" style="width: 280px;">

    <ul class="nav nav-pills flex-column mb-auto">
      <li class="nav-item">
        <a href="#" class="nav-link active" aria-current="page">
          <i class="fa fa-home"></i>
          Dashboard
        </a>
      </li>
      <li>
        <a href="./items.php" class="nav-link link-dark">
          <i class="fa-solid fa-list-ul"></i>
          Sales by Item
        </a>
      </li>
      <li>
        <a href="#" class="nav-link link-dark">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#table"/></svg>
          Orders
        </a>
      </li>
      <li>
        <a href="./items.php" class="nav-link link-dark">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="./items.php"/></svg>
          Items
        </a>
      </li>
      <li>
        <a href="customers.php" class="nav-link link-dark">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="customers.php"/></svg>
          Customers
        </a>
      </li>
    </ul>
    <hr>
    <div class="dropdown">
      <a href="#" class="d-flex align-items-center link-dark text-decoration-none dropdown-toggle" id="dropdownUser2" data-bs-toggle="dropdown" aria-expanded="false">
        <img src="https://github.com/mdo.png" alt="" width="32" height="32" class="rounded-circle me-2">
        <strong>Manager</strong>
      </a>
      <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2">
        <li><a class="dropdown-item" href="#">New project...</a></li>
        <li><a class="dropdown-item" href="#">Settings</a></li>
        <li><a class="dropdown-item" href="#">Profile</a></li>
        <li><hr class="dropdown-divider"></li>
        <li><a class="dropdown-item" href="#">Sign out</a></li>
      </ul>
    </div>
  </div>

<div class="col-md-9">
    <div class="d-flex justify-content-center mt-5">
    <?php
include("connect.php");
$id=$_GET['id'];
$sql = "SELECT * FROM customers WHERE id=" .$id;
$results = mysqli_query($conn, $sql);
$row=mysqli_fetch_array($results);
?>
        <form  action="edit_customer.php" method="post">
          <h3>Edit Customer</h3>
          <p class="text-muted">Kindly use the following form to register a new customer.</p>
          <div class="mb-3">
            <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
            <input type="text" class="form-control" value="<?php echo $row['name']?>" name="name" placeholder="First Name" id=""> </div>
            <div class="mb-3">
            <input type="text" class="form-control" value="<?php echo $row['last_name']?>" name="last_name" placeholder="Last name" id=""></div>
            <div class="mb-3">
            <input type="text" class="form-control" value="<?php echo $row['email']?>" name="email" placeholder="Enter email" id=""></div>
            <div class="mb-3">
            <input type="text" class="form-control" value="<?php echo $row['phone']?>" name="phone" placeholder="Mobile Number" id=""></div>
            <div class="d-flex justify-content-between mb-3">
            <select class="form-select form-control" value="<?php echo $row['gender']?>" name="gender" aria-label="Default select example" style="margin: right 150px;">
              <option selected>Gender</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select> </div>
            <div class="mb-3">
            <input type="text" class="form-control" value="<?php echo $row['age']?>" name="age" placeholder="Age" id=""></div>
            <div class="mb-3">
            <input type="text" class="form-control" value="<?php echo $row['location']?>" name="location" placeholder="Location" id=""></div>
            <div class="d-flex justify-content-end mt-3">
                <button type="submit" class="btn btn-success text-uppercase">Save</button>
            </div>
          </div>
        
        </form>   
      
        </div>
      </div>




    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>

    <script src="sidebars.js"></script>
  </body>
</html>
